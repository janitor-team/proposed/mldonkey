# Swedish po-debconf for mldonkey
# Copyright (C) 2008 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the mldonkey package.
# Martin Bagge <brother@bsnet.se>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: mldonkey\n"
"Report-Msgid-Bugs-To: mldonkey@packages.debian.org\n"
"POT-Creation-Date: 2011-05-07 15:41+0200\n"
"PO-Revision-Date: 2008-08-01 02:18+0100\n"
"Last-Translator: Martin Bagge <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Swedish\n"
"X-Poedit-Country: Sweden\n"

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid "Launch MLDonkey at startup?"
msgstr "Starta MLDonkey vid systemets uppstart?"

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid ""
"If enabled, each time your machine starts, the MLDonkey server will be "
"started."
msgstr ""
"Om detta alternativ aktiveras kommer MLDonkey-server att startas varje gång "
"maskinen startas."

#. Type: boolean
#. Description
#: ../mldonkey-server.templates:1001
msgid ""
"Otherwise, you will need to launch MLDonkey yourself each time you want to "
"use it."
msgstr ""
"I annat fall måste du starta MLDonkey manuellt varje gång du vill använda "
"det."

#~ msgid "Bug #200500"
#~ msgstr "Bugg #200500"

#~ msgid ""
#~ "Previous versions of mldonkey-server suffer from a serious DFSG policy "
#~ "violation."
#~ msgstr "Tidigare version av mldonkey-server bröt mot DFSG."

#~ msgid ""
#~ "The plugin for the fasttrack protocol (e.g. used by kazaa) of mldonkey-"
#~ "server was made with illegal coding practice. This version fixes the "
#~ "problem by removing this plugin from the MLDonkey package. Any fasttrack "
#~ "sources will be filtered out of your files.ini."
#~ msgstr ""
#~ "Insticket för fasttrack-protokollet (användas av bl.a. Kazaa) för "
#~ "mldonkey-server togs fram på ett felaktigt sätt. Den här versionen "
#~ "åtgärdar detta problemet genom att insticket tas bort från paketet. "
#~ "Fasttrack-källor kommer att filtreras bort från din files.ini."

#~ msgid ""
#~ "Your entire fasttrack upload will disappear with the next restart of the "
#~ "mldonkey server."
#~ msgstr ""
#~ "Hela din kö med fasttrack-uppladdningar kommer att försvinna när mldonkey-"
#~ "server startas om nästa gång."

#~ msgid ""
#~ "See /usr/share/doc/mldonkey-server/README.Debian for more information."
#~ msgstr ""
#~ "Läs mer i /usr/share/doc/mldonkey-server/README.Debian för ytterligare "
#~ "information."

#~ msgid "MLDonkey user:"
#~ msgstr "MLDonkey-användare:"

#~ msgid "Define the user who will run the MLDonkey server process."
#~ msgstr "Ange vilken användare som ska köra MLDonkey-servern."

#~ msgid ""
#~ "Please do not choose a real user. For security reasons it is better if "
#~ "this user does not own any other data than the MLDonkey share."
#~ msgstr ""
#~ "Ange ingen riktig användare. Av säkerhets skäl är det bättre om "
#~ "användaren inte har några data utöver de som MLDonkey håller reda på."

#~ msgid ""
#~ "You will use this user account to share and get data from the peer-to-"
#~ "peer networks."
#~ msgstr ""
#~ "Du kommer att använda detta konto för att dela och ta emot data från peer-"
#~ "to-peer nätverk."

#~ msgid ""
#~ "This user will be a system user (if created). You won't be able to login "
#~ "into your system with this user name."
#~ msgstr ""
#~ "Denna användare kommer att bli en systemanvändare (om den skapas). Du kan "
#~ "inte logga in med den i ditt system."

#~ msgid "MLDonkey group:"
#~ msgstr "Grupp för MLDonkey:"

#~ msgid "Define the group which will run the MLDonkey server process."
#~ msgstr "Ange gruppen som kommer att köra MLDonkey-servern-"

#~ msgid ""
#~ "Please do not choose a real group. For security reasons it is better if "
#~ "this group does not own any other data than the MLDonkey share."
#~ msgstr ""
#~ "Ange ingen riktig grupp. Av säkerhetsskäl är det bättre om gruppen inte "
#~ "har några data utöver de som MLDonkey håller reda på."

#~ msgid ""
#~ "Users of this group can start and stop the MLDonkey server and can also "
#~ "access the files fetched from the peer-to-peer networks."
#~ msgstr ""
#~ "Användare i den här gruppen kan starta och stoppa MLDonkey-servern och "
#~ "har också tillgång till filerna som kommer från peer-to-peer nätverken."

#~ msgid "Change the owner of old files?"
#~ msgstr "Byta ägare på gamla filer?"

#~ msgid ""
#~ "You have changed the MLDonkey user. You can change the ownership of your "
#~ "files to the new user."
#~ msgstr ""
#~ "Du har bytt MLDonkey-användare. Du kan byta ägarskapet på dina filer till "
#~ "den nya användaren."

#~ msgid ""
#~ "PS: the former user won't be deleted from /etc/passwd, you will have to "
#~ "do it yourself later (e.g. with deluser(8)), or you keep it along with "
#~ "the old configuration."
#~ msgstr ""
#~ "OBS: den gamla användaren kommer inte att tas bort ut /etc/passwd, du "
#~ "måste göra detta själv vid ett senare tillfälle (genom att använda deluser"
#~ "(8)) eller behålla den tillsammans med dina gamla inställningar.."

#~ msgid "MLDonkey directory:"
#~ msgstr "Sökväg till MLDonkey:"

#~ msgid ""
#~ "Define the directory to which the MLDonkey server will be chdired and "
#~ "chrooted."
#~ msgstr ""
#~ "Ange sökvägen som MLDonkey kommer att stängas in i med hjälp av chroot."

#~ msgid ""
#~ "The .ini configuration files, incoming and shared directories will be in "
#~ "this directory."
#~ msgstr ""
#~ ".ini-filer och inställningsfiler samt inkommande och utdelade kataloger "
#~ "kommer att finnas i den här katalogstrukturen."

#~ msgid ""
#~ "Chroot support is not complete. For now, chroot is not possible, but it "
#~ "may be enabled in the near future."
#~ msgstr ""
#~ "Stödet för chroot är inte fullständigt. Det är inte möjligt att använda "
#~ "chroot men det kan bli aktiverat i en nära framtid."

#~ msgid "Move the old configuration?"
#~ msgstr "Flytta gamla inställningsfiler?"

#~ msgid ""
#~ "You have changed the mldonkey directory. You can move the old files to "
#~ "this new directory."
#~ msgstr ""
#~ "Du har bytt sökväg för mldonkey. Du kan flytta den gamla filerna till "
#~ "denna nya katalog."

#~ msgid ""
#~ "If you choose no, the old directory won't be deleted. You will have to do "
#~ "it yourself."
#~ msgstr ""
#~ "Om du väljer nej så kommer den gamla sökvägen inte att tas bort. Då måste "
#~ "du göra detta själv."

#~ msgid "Niceness of MLDonkey:"
#~ msgstr "Nice-värde för MLDonkey:"

#~ msgid ""
#~ "MLDonkey uses heavy calculation from time to time (like hashing very big "
#~ "files). It should be a good idea to set a very kind level of niceness, "
#~ "depending on what ressources you want to give to MLDonkey."
#~ msgstr ""
#~ "Då MLDonkey använder en hel del tunga beräkningar från tid till annan "
#~ "(när stora filer hashas) så är det en god idé att ange en bra nivå på "
#~ "Nice. Detta begränsar hur mycket av systemets resurser som reserveras "
#~ "till MLDonkey."

#~ msgid ""
#~ "You can set values from -20 to 20. The bigger the niceness, the lower the "
#~ "priority of MLDonkey processes."
#~ msgstr ""
#~ "Värdet kan vara -20 till 20. Ju högre Nice-nivå ju lägre prioritet ges "
#~ "till MLDonkey-processen."

#~ msgid "Maximal download speed (kB/s):"
#~ msgstr "Maximal nedladdningshastighet (kB/s):"

#~ msgid ""
#~ "Set the maximal download rate. It can be useful to limit this rate, in "
#~ "order to always have a minimal bandwidth for other internet applications."
#~ msgstr ""
#~ "Ange den maximala nedladdningshastigheten. Det kan vara bra att ställa in "
#~ "en gräns för detta för att andra Internetapplikationer alltid ska vara "
#~ "garanterade en del av bandbredden."

#~ msgid ""
#~ "It has also been noticed that a full use of the bandwidth could cause "
#~ "problems with DSL connection handling. This is not a rule, it is just "
#~ "based on a few experiments."
#~ msgstr ""
#~ "Dessutom har det uppdagats att fullständigt utnyttjaden av bandbredden "
#~ "kan orsaka problem för DSL-baserade uppkopplingar. Detta är ingen regel "
#~ "utan baseras på experimenterande."

#~ msgid "0 means no limit."
#~ msgstr "0 innebär obegränsat."

#~ msgid "Maximal upload speed (kB/s):"
#~ msgstr "Maximal uppladdningshastighet (kB/s):"

#~ msgid ""
#~ "Set the maximal upload rate. You must keep in mind that a peer-to-peer "
#~ "network is based on sharing. Do not use a very low rate."
#~ msgstr ""
#~ "Ange den maximala uppladdningshastigheten. Kom ihåg att peer-to-peer-"
#~ "nätverk baseras på delning. Ange inte ett för lågt värde."

#~ msgid ""
#~ "Some networks calculate the download credit by the upload rate. More "
#~ "upload speed means more download speed."
#~ msgstr ""
#~ "En del nätverk baserar sin nedladdningen på uppladdningen. Ju högre "
#~ "uppladningshastighet desto högre nedladdningshastighet."

#~ msgid ""
#~ "As for the download speed, you should limit this rate so that you can "
#~ "still use the internet even when MLDonkey is running."
#~ msgstr ""
#~ "Vad gäller nedladdningshastighet så ska du begränsa denna så att du "
#~ "fortfarande kan använda Internet när MLDonkey används."

#~ msgid "Password of admin user:"
#~ msgstr "Lösenord för administrationsanvändaren:"

#~ msgid ""
#~ "As of version 2.04rc1, a new user management appears. The password is "
#~ "encrypted and stored in downloads.ini."
#~ msgstr ""
#~ "I och med version 2.04rc1 så introduceras en ny användarhantering. "
#~ "Lösenordet lagras krypterat i downloads.ini."

#~ msgid ""
#~ "If you want to add a new user for MLDonkeys user management or want to "
#~ "change the password, refer to /usr/share/doc/mldonkey-server/README."
#~ "Debian."
#~ msgstr ""
#~ "Om du vill lägga till en ny användare för MLDonkeys användarhantering "
#~ "eller vill byta lösenordet ska du läsa hur i /usr/share/doc/mldonkey-"
#~ "server/README.Debian."

#~ msgid "Retype password of the admin user:"
#~ msgstr "Upprepa lösenordet för adminstratören:"

#~ msgid "Please confirm your admin's password."
#~ msgstr "Ange lösenordet för administratören en gång till."

#~ msgid "Passwords do not match"
#~ msgstr "Lösenorden stämmer inte överrens."

#~ msgid "The two password you enter must be the same."
#~ msgstr "De båda lösenorden måste vara likadana."

#~ msgid "You will be asked until you can provide the same password twice."
#~ msgstr ""
#~ "Du kommer att få försöka igen tills lösenordet skrivs likadant två gånger "
#~ "i följd."
