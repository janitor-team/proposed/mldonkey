(* This program is a test program for mldonkey_server *)

let _ = 
  Sys.set_signal Sys.sigchld (Sys.Signal_handle ( fun x ->
    print_string "Receive SIGCHLD"; print_newline (); exit 1 ));
  Sys.set_signal Sys.sighup (Sys.Signal_handle ( fun x ->
    print_string "Receive SIGHUP"; print_newline () ));
  Sys.set_signal Sys.sigterm (Sys.Signal_handle ( fun x ->
    print_string "Receive SIGTERM"; print_newline (); exit 2 ));
  Sys.set_signal Sys.sigpipe (Sys.Signal_handle ( fun x ->
    print_string "Receive SIGPIPE"; print_newline (); exit 3 ));
  Sys.set_signal Sys.sigint (Sys.Signal_handle ( fun x ->
    print_string "Receive SIGINT"; print_newline (); exit 4 ));
  print_string "MLnet pathological pseudo server";
  print_newline ();
  Array.iter ( fun x -> print_string (x^" ") ) Sys.argv;
  print_newline ();  
  Arg.parse
  [
  ("-stop", Arg.Int ( fun x -> 
    print_string ("Exiting ( "^(string_of_int x)^" )");
    print_newline ();
    exit x ), 
    "Stop with exit number");
  ("-sleep", Arg.Int ( fun x -> 
    print_string ("Sleeping ( "^(string_of_int x)^" )");
    print_newline ();
    ignore (Unix.sleep x) ),
    "Sleep for x seconds");
  ("-iter_stdout", Arg.Int ( fun x -> 
    for i = 0 to x do
    begin
      print_int i;
      print_newline ()
    end
    done ),
    "Print list of int ( from 0 to x ) to stdout");
  ("-iter_stderr", Arg.Int ( fun x ->
    for i = 0 to x do
    begin
      prerr_int i;
      prerr_newline ()
    end
    done ),
    "Print list of int ( from 0 to x ) to stderr");
  ]
  ( fun x -> () )
  "Usage : mlnet_pathological [options]\n"
  
    
      
      
  
