type state_mldonkey_submit = {
  mutable program : string;
  mutable prg_args : string list;
  mutable ed2k : string;
  }

let create_state_mldonkey_submit () = {
  program = "/usr/bin/mldonkey_command";
  prg_args = [];
  ed2k = "";
  }

let _ = 
  let state = create_state_mldonkey_submit ()
  in
  let _ = Arg.parse [
    ("--", Arg.Rest ( fun x -> state.prg_args <- x :: state.prg_args  ),
      "Argument to pass to mldonkey_command");
    ("--program", Arg.String ( fun x -> state.program <- x ),
      "Name of the mldonkey_command program");
    ]
    (fun x -> state.ed2k <- x)
    "Usage : mldonkey_submit [options] ed2k://... -- [mldonkey_command options] :\n"
  in
  match state.ed2k with
  "" ->
    prerr_string "You must provide an ed2k link";
    prerr_newline ()
  | _ ->
    let args = Array.of_list (state.program :: 
      (List.rev (("dllink "^state.ed2k) :: state.prg_args)))
    in
    Unix.execv state.program args
  
